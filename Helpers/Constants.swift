//
//  Constants.swift
//  UberdooX
//
//  Created by Karthik Sakthivel on 21/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Constants {
    //App Constants
    static var baseURL = "http://35.153.186.250/uber_test/public/provider"
    static var adminBaseURL = "http://35.153.186.250/uber_test/public/admin"
    static var mapsKey = "AIzaSyDXJKnPotWb4bI0kajz24H7ZB2XNrO0QX4"
    static var placesKey = "AIzaSyDXJKnPotWb4bI0kajz24H7ZB2XNrO0QX4"
    static var locations : [JSON] = []
    static var timeSlots : [JSON] = []
}
func validateEmail(enteredEmail:String) -> Bool {
    
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: enteredEmail)
    
}

