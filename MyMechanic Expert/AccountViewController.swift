//
//  AccountViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
import Nuke
import UserNotifications

class AccountViewController:UIViewController,UITableViewDelegate,UITableViewDataSource,UNUserNotificationCenterDelegate,updateImageDelegate {
   
    func updateImage() {
        
        if let image = UserDefaults.standard.string(forKey: "image") as String!{
            
            if let imageUrl = URL.init(string: image) as URL!{
                Nuke.loadImage(with: imageUrl, into: self.userPicture)
            }
        }
        if let first_name = UserDefaults.standard.string(forKey: "first_name") as String!
        {
            if let  last_name = UserDefaults.standard.string(forKey: "last_name") as String!{
                nameLbl.text = "\(String(describing: first_name)) \(String(describing: last_name))"
            }
            else{
                nameLbl.text = "\(String(describing: first_name))"
            }
        }
        else{
            nameLbl.text = "Guest User"
        }
        
    }
    
    
    @IBOutlet weak var btnClosetheme: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userPicture: UIImageView!
    var titles :[String] = []
    var images :NSArray!
    
    @IBOutlet weak var VwchangeTheme: UIView!
    
    @IBOutlet weak var btnclr1: UIButton!
    @IBOutlet weak var btnclr2: UIButton!
    @IBOutlet weak var btnclr6: UIButton!
    @IBOutlet weak var btnclr5: UIButton!
    @IBOutlet weak var btnclr4: UIButton!
    @IBOutlet weak var btnclr3: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    var color1: UIColor = UIColor.init(red: 107 / 255, green: 127 / 255, blue: 252 / 255, alpha: 1.0)
    var color2: UIColor = UIColor.init(red: 252 / 255, green: 107 / 255, blue: 180 / 255, alpha: 1.0)
    var color3: UIColor = UIColor.init(red: 48 / 255, green: 58 / 255 , blue: 82 / 255, alpha: 1.0)
    var color4: UIColor  = UIColor.init(red: 54 / 255, green: 209 / 255, blue: 196 / 255, alpha: 1.0)
    var color5: UIColor = UIColor.init(red: 165 / 255, green: 107 / 255, blue: 252 / 255, alpha: 1.0)
    var color6: UIColor = UIColor.init(red: 252 / 255, green: 107 / 255, blue: 107 / 255 , alpha: 1.0)
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()
         VwchangeTheme.isHidden = true
        btnClosetheme.isHidden = true
        if let image = UserDefaults.standard.string(forKey: "image") as String!{
            
            if let imageUrl = URL.init(string: image) as URL!{
                Nuke.loadImage(with: imageUrl, into: self.userPicture)
            }
        }
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate  = self
        }
        
        if let first_name = UserDefaults.standard.string(forKey: "first_name") as String!
        {
            if let  last_name = UserDefaults.standard.string(forKey: "last_name") as String!{
                nameLbl.text = "\(String(describing: first_name)) \(String(describing: last_name))"
            }
            else{
                nameLbl.text = "\(String(describing: first_name))"
            }
        }
        else{
            nameLbl.text = "Guest User"
        }
        btnclr1.layer.cornerRadius = btnclr1.frame.size.width / 2
        btnclr1.layer.masksToBounds = true
        btnclr2.layer.cornerRadius = btnclr1.frame.size.width / 2
        btnclr2.layer.masksToBounds = true
        btnclr3.layer.cornerRadius = btnclr1.frame.size.width / 2
        btnclr3.layer.masksToBounds = true
        btnclr4.layer.cornerRadius = btnclr1.frame.size.width / 2
        btnclr4.layer.masksToBounds = true
        btnclr5.layer.cornerRadius = btnclr1.frame.size.width / 2
        btnclr5.layer.masksToBounds = true
        btnclr6.layer.cornerRadius = btnclr1.frame.size.width / 2
        btnclr6.layer.masksToBounds = true
        
        tableView.delegate = self
        tableView.dataSource = self
        let isLoggedInSkipped = UserDefaults.standard.bool(forKey: "isLoggedInSkipped")
        if(isLoggedInSkipped)
        {
            self.titles = NSArray(objects: "About us","Help and FAQ","Login") as! [String]
            self.images = NSArray(objects:"about_us","help","log_out")
        }
        else{
            self.titles = NSArray(objects: "Profile","Change Password","Address","Services","Schedule","About Us","Change Theme","Logout") as! [String]//"Profile",
            self.images = NSArray(objects:"new_profile","new_change_passsword","new_address","new_services","new_schedule","new_about_us","Themes-1","new_logout")//"new_profile",
        }
        
        
        
       
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let image = UserDefaults.standard.string(forKey: "image") as String!{
            
            if let imageUrl = URL.init(string: image) as URL!{
                Nuke.loadImage(with: imageUrl, into: self.userPicture)
            }
            else if UserDefaults.standard.object(forKey: "myColor") != nil
            {
                //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
                let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
                //            var color: UIColor? = nil
                mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
                changeTintColor(userPicture, arg: mycolor)
                tabBarController?.tabBar.tintColor = mycolor
            }
        }
        else if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            changeTintColor(userPicture, arg: mycolor)
        }
        
    }
    func chageColor()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnclr2(_ sender: Any)
    {
        let colorData = NSKeyedArchiver.archivedData(withRootObject: color2)
        UserDefaults.standard.set(colorData, forKey: "myColor")
        VwchangeTheme.isHidden = true
        
        self.chageColor()
    }
    
    @IBAction func btnclr3(_ sender: Any)
    {
        let colorData = NSKeyedArchiver.archivedData(withRootObject: color3)
        UserDefaults.standard.set(colorData, forKey: "myColor")
        VwchangeTheme.isHidden = true
        
        self.chageColor()
    }
    
    @IBAction func btnclr6(_ sender: Any)
    {
        let colorData = NSKeyedArchiver.archivedData(withRootObject: color6)
        UserDefaults.standard.set(colorData, forKey: "myColor")
        VwchangeTheme.isHidden = true
        
        self.chageColor()
    }
    
    @IBAction func btnclr5(_ sender: Any)
    {
        let colorData = NSKeyedArchiver.archivedData(withRootObject: color5)
        UserDefaults.standard.set(colorData, forKey: "myColor")
        VwchangeTheme.isHidden = true
        
        self.chageColor()
    }
    
    @IBAction func btnclr4(_ sender: Any)
    {
        let colorData = NSKeyedArchiver.archivedData(withRootObject: color4)
        UserDefaults.standard.set(colorData, forKey: "myColor")
        VwchangeTheme.isHidden = true
        
        self.chageColor()
    }
    
    @IBAction func btnclr1(_ sender: Any)
    {
        let colorData = NSKeyedArchiver.archivedData(withRootObject: color1)
        UserDefaults.standard.set(colorData, forKey: "myColor")
        VwchangeTheme.isHidden = true
        
        self.chageColor()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTableViewCell", for: indexPath) as! AccountTableViewCell
        cell.title.text = self.titles[indexPath.row]
        let imagename = self.images.object(at: indexPath.row) as! String
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            
           
            let imagename = self.images.object(at: indexPath.row) as! String
              cell.icon.image = UIImage(named:imagename)
             changeTintColor(cell.icon, arg: mycolor)
        }
        else {
            let imagename = self.images.object(at: indexPath.row) as! String
            cell.icon.image = UIImage(named:imagename)
        }
      
        
        return cell
        
    }
    func changeTintColor(_ img: UIImageView?, arg color: UIColor?) {
        if let aColor = color {
            img?.tintColor = aColor
        }
        var newImage: UIImage? = img?.image?.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions((img?.image?.size)!, false, (img?.image?.scale)!)
        color?.set()
        newImage?.draw(in: CGRect(x: 0, y: 0, width: img?.image?.size.width ?? 0.0, height: img?.image?.size.height ?? 0.0))
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        img?.image = newImage
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titles.count
    }
    
    @IBAction func btnclose(_ sender: Any) {
        btnClosetheme.isHidden = true
        VwchangeTheme.isHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.titles = NSArray(objects: "View Profile","Change Password","About Us","Help and FAQ","Logout")
        
        if(self.titles[indexPath.row] == "Profile")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            
            vc.updateDelegate = self
            
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "Change Password")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "About Us")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            vc.titleString = "About Us"
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "Address")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "Change Theme")
        {
           VwchangeTheme.isHidden = false
            btnClosetheme.isHidden = false
        }
        else if(self.titles[indexPath.row] == "Services"){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServicesViewController") as! ServicesViewController
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "Schedule"){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedulesViewController") as! SchedulesViewController
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "Logout" || self.titles[indexPath.row] == "Login")
        {
            print(self.titles[indexPath.row])
            let isLoggedInSkipped = UserDefaults.standard.bool(forKey: "isLoggedInSkipped")
            if(isLoggedInSkipped)
            {
                self.login()
            }
            else{
                self.logout()
            }
        }
        
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func login(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
        self.present(vc, animated: true, completion: nil)
    }
    func logout()
    {
        let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        print(accesstoken!)
        let headers: HTTPHeaders = [
            "Authorization": accesstoken!,
            "Accept": "application/json"
        ]
        
        SwiftSpinner.show("Logging out...")
        let url = "\(Constants.baseURL)/logout"
        Alamofire.request(url,method: .get,headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("LOGOUT JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    if(jsonResponse["error"].stringValue == "Unauthenticated" || jsonResponse["error"].stringValue == "true")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func termsAndConditions(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.titleString = "Terms & Conditions"
        MainViewController.changePage = false
        self.present(vc, animated: true, completion: nil)
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
    }
    
}

