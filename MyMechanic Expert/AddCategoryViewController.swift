//
//  AddCategoryViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 07/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner


protocol AddressSelectionDelegate: class {
    func didFinishSelectingCategories(selctedCategory: JSON)
}

class AddCategoryViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
   
    

    weak var delegate: AddressSelectionDelegate?
    
    @IBOutlet weak var quickPitchFld: UITextField!
    @IBOutlet weak var pricePerHourFld: UITextField!
    @IBOutlet weak var experienceFld: UITextField!
    @IBOutlet weak var subcategoryFld: UITextField!
    @IBOutlet weak var categoryFld: UITextField!
    
    var listCategories : [JSON] = []
    var listSubCategories : [JSON] = []
    var category : JSON!  = JSON.init()
    var mycolor = UIColor()
    var selectedCategoryId = "0"
    var selectedSubCategoryId = "0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategories()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            
            
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getCategories(){
        
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
            print(accesstoken)
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let url = "\(Constants.baseURL)/listcategory"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                if let json = response.result.value {
                    print("CATEGORIES JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        
                        self.listCategories = jsonResponse["list_category"].arrayValue;
                        if(self.listCategories.count > 0){
                            print(self.listCategories)
                        }
                    }
                }
            }
            else{
                print(response.error!.localizedDescription)
                //                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func pressedOutside(_ sender: Any) {
        self.dismiss(animated: true, completion:nil)
    }
    @IBAction func categoryButtonPressed(_ sender: Any) {
        showPickerInActionSheet(sentBy: "category")
    }
    
    @IBAction func subCategoyButtonPressed(_ sender: Any) {
        showPickerInActionSheet(sentBy:"subCategory")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func donePressed(_ sender: Any) {
        
        if(self.categoryFld?.text?.count ==  0)
        {
            self.showAlert(title: "Validation Failed", msg: "Select a category")
        }
        else if(self.subcategoryFld?.text?.count ==  0)
        {
            self.showAlert(title: "Validation Failed", msg: "Select a sub category")
        }
        else if(self.experienceFld?.text?.count ==  0)
        {
            self.showAlert(title: "Validation Failed", msg: "Experience required")
        }
        else if(self.pricePerHourFld?.text?.count == 0)
        {
            self.showAlert(title: "Validation Failed", msg: "Price per hour required")
        }
        else if(self.quickPitchFld?.text?.count == 0)
        {
            self.showAlert(title: "Validation Failed", msg: "Quick Pitch required")
        }
        else{
            if(self.delegate != nil){
                self.category["id"].stringValue = self.selectedCategoryId
                self.category["category_id"].stringValue = self.selectedCategoryId
                self.category["sub_category_id"].stringValue = self.selectedSubCategoryId
                self.category["quickpitch"].stringValue = self.quickPitchFld.text!
                self.category["priceperhour"].stringValue = self.pricePerHourFld.text!
                self.category["experience"].stringValue = self.pricePerHourFld.text!
                self.category["categoryName"].stringValue = self.categoryFld.text!
                self.category["subCategoryName"].stringValue = self.subcategoryFld.text!
                self.delegate!.didFinishSelectingCategories(selctedCategory: self.category)
                self.dismiss(animated: true, completion:nil)
            }
        }
        
    }
    
    func showPickerInActionSheet(sentBy: String) {
        let title = ""
        let message = "\n\n\n\n\n\n\n\n\n\n";
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.actionSheet);
        alert.isModalInPopover = true;
        
        
        //Create a frame (placeholder/wrapper) for the picker and then create the picker
        let pickerFrame: CGRect = CGRect.init(x:-10, y:52,width: self.view.frame.size.width ,height: 100); // CGRectMake(left), top, width, height) - left and top are like margins
        let picker: UIPickerView = UIPickerView(frame: pickerFrame);

// If there will be 2 or 3 pickers on this view, I am going to use the tag as a way
// to identify them in the delegate and datasource. /* This part with the tags is not required.
// I am doing it this way, because I have a variable, witch knows where the Alert has been invoked from.

        if(sentBy == "category"){
            picker.tag = 1;
        } else if (sentBy == "subCategory"){
            picker.tag = 2;
        } else {
            picker.tag = 0;
        }
 
        //set the pickers datasource and delegate
        picker.delegate = self;
        picker.dataSource = self;
 
        //Add the picker to the alert controller
        alert.view.addSubview(picker);
 
        //Create the toolbar view - the view witch will hold our 2 buttons
        let toolFrame = CGRect.init(x:0,y: 5,width: self.view.frame.size.width, height:45);
        let toolView: UIView = UIView(frame: toolFrame);
 
        //add buttons to the view
        let buttonCancelFrame: CGRect = CGRect.init(x:0,y: 7,width: self.view.frame.size.width/2, height:30); //size & position of the button as placed on the toolView
 
        //Create the cancel button & set its title
        let buttonCancel: UIButton = UIButton(frame: buttonCancelFrame);
        buttonCancel.setTitle("Cancel", for: UIControlState.normal);
        buttonCancel.setTitleColor(UIColor.blue, for: UIControlState.normal);
        toolView.addSubview(buttonCancel); //add it to the toolView
 
        //Add the target - target, function to call, the event witch will trigger the function call
        buttonCancel.addTarget(self, action: #selector(AddCategoryViewController.cancelSelection), for: UIControlEvents.touchDown);
 
 
        //add buttons to the view
        let buttonOkFrame: CGRect = CGRect.init(x:(self.view.frame.size.width/2)-10, y:7, width:(self.view.frame.size.width/2), height:30); //size & position of the button as placed on the toolView
 
        //Create the Select button & set the title
        let buttonOk: UIButton = UIButton(frame: buttonOkFrame);
        buttonOk.setTitle("OK", for: UIControlState.normal);
        buttonOk.setTitleColor(UIColor.blue, for: UIControlState.normal);
        toolView.addSubview(buttonOk); //add to the subview
 
        buttonOk.addTarget(self, action: #selector(AddCategoryViewController.okSelection), for: UIControlEvents.touchDown);
 
        //add the toolbar to the alert controller
        alert.view.addSubview(toolView);
 
        self.present(alert, animated: true, completion: nil);
 }
 

    @objc func okSelection(sender: UIButton){
        self.dismiss(animated: true, completion: nil);
    }
    
    @objc func cancelSelection(sender: UIButton){
        print("Cancel");
        self.dismiss(animated: true, completion: nil);
    }
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
 // returns number of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if(pickerView.tag == 1){
        return self.listCategories.count;
    } else if(pickerView.tag == 2){
        return self.listSubCategories.count;
    } else  {
        return 0;
    }
 }
 
 // Return the title of each row in your picker ... In my case that will be the profile name or the username string
 func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    if(pickerView.tag == 1){
        return self.listCategories[row]["category_name"].stringValue
    }
    else if(pickerView.tag == 2)
    {
        return self.listSubCategories[row]["sub_category_name"].stringValue
    }
        return "";
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView.tag == 1){
            self.listSubCategories = self.listCategories[row]["list_subcategory"].arrayValue
            self.categoryFld.text = self.listCategories[row]["category_name"].stringValue
            self.selectedCategoryId = self.listCategories[row]["id"].stringValue
            self.selectedSubCategoryId = "0"
            self.subcategoryFld.text = ""
        }
        else if(pickerView.tag == 2){
            if(row < self.listSubCategories.count){
                self.selectedCategoryId = self.listSubCategories[row]["category_id"].stringValue
                self.selectedSubCategoryId = self.listSubCategories[row]["id"].stringValue
                self.subcategoryFld.text = self.listSubCategories[row]["sub_category_name"].stringValue
            }
        }

        print(self.selectedCategoryId)
        print(self.selectedSubCategoryId)
    }
 
 }
 

