//
//  BookingDetailsViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
import Nuke

class BookingDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var bookingDetails : [String:JSON]!
    
    @IBOutlet weak var imggps: UIImageView!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var directionsButton: UIButton!
    var timer : Timer!
    @IBOutlet weak var bookingStatus: UILabel!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var serviceStatusTableConstraintConstant: NSLayoutConstraint!

    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var tax: UILabel!
    @IBOutlet weak var priceViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var workingHours: UILabel!
    @IBOutlet weak var bookingAddress: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var bookingId: UILabel!
    @IBOutlet weak var bookingName: UILabel!
    @IBOutlet weak var subCategoryName: UILabel!
    @IBOutlet weak var mapImage: UIImageView!
    
    @IBOutlet weak var elapsedTimeView: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var hourDigit1: UILabel!
    @IBOutlet weak var hourDigit2: UILabel!
    @IBOutlet weak var minuteDigit1: UILabel!
    @IBOutlet weak var minuteDigit2: UILabel!
    @IBOutlet weak var secondsDigit1: UILabel!
    @IBOutlet weak var secondsDigit2: UILabel!
    
    @IBOutlet weak var elapsedTimeLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    
    var statusArray : [String] = []
    var timeStampArray : [String] = []
    var mycolor = UIColor()
    @IBOutlet weak var serviceStatusTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(bookingDetails)
        serviceStatusTableView.delegate = self
        serviceStatusTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let lat = self.bookingDetails["user_latitude"]!.doubleValue
        let long = self.bookingDetails["user_longitude"]!.doubleValue
        
        let styleMapUrl: String = "https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDcuEgb-Jr0-QKM6SdFd3WxCPhEeukUHtM&center=\(lat),\(long)&zoom=15&format=jpeg&maptype=roadmap&size=\(2 * Int(self.mapImage.frame.size.width))x\(2 * Int(self.mapImage.frame.size.height))&sensor=true"
        
        statusArray.removeAll()
        timeStampArray.removeAll()
        
        switch bookingDetails["status"]!.stringValue {
        case "Completedjob":
            bookingStatus.text = "Payment pending"
            bookingStatus.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
            
        case "Waitingforpaymentconfirmation":
            bookingStatus.text = "Payment pending"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
            
        case "Reviewpending":
            bookingStatus.text = "Review pending"
            bookingStatus.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
            
        case "Finished":
            bookingStatus.text = "Job Completed"
            bookingStatus.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
        case "StarttoCustomerPlace":
            bookingStatus.text = "On the way"
            bookingStatus.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            
        case "Startedjob":
            bookingStatus.text = "Work in progress"
            bookingStatus.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
        case "Pending":
            bookingStatus.text = "Pending"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            
        case "Accepted":
            bookingStatus.text = "Accepted"
            bookingStatus.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
//            cancelView.isHidden = false
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            
        case "Rejected":
            bookingStatus.text = "Rejected"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Rejected")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Rejected_time"]!.stringValue)
            
        case "CancelledbyProvider":
            bookingStatus.text = "Cancelled by Provider"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Cancelled by Provider")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["CancelledbyProvider_time"]!.stringValue)
        case "CancelledbyUser":
            bookingStatus.text = "Cancelled by User"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Cancelled by User")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["CancelledbyUser_time"]!.stringValue)
        default:
            bookingStatus.text = "Pending"
            cancelView.isHidden = true
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            statusArray.append("Service Requested")
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
        }
        
        print(styleMapUrl)
        if let url = URL(string: styleMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        {
            Nuke.loadImage(with: url, into: self.mapImage)
        }
        
        // Do any additional setup after loading the view.
        if(bookingDetails["status"]?.stringValue == "Completedjob" || bookingDetails["status"]?.stringValue == "Waitingforpaymentconfirmation" ||
            bookingDetails["status"]?.stringValue == "Reviewpending" ||
            bookingDetails["status"]?.stringValue == "Finished")
        {
            self.workingHours.isHidden = false
            self.taxLbl.isHidden = false
            self.tax.isHidden = false
            self.total.isHidden = false
            self.button.isHidden = true
            
            self.elapsedTimeView.isHidden = true
            
            self.total.text = "€ \(bookingDetails["total_cost"]!.stringValue)"
            self.tax.text = bookingDetails["gst_cost"]!.stringValue
            self.taxLbl.text = "\(bookingDetails["tax_name"]!.stringValue) (\(bookingDetails["gst_percent"]!.stringValue)%)"
            
            
            let workhrs = minutesToHoursMinutes(minutes: bookingDetails["worked_mins"]!.intValue)
            
            if(workhrs.hours > 0){
                self.workingHours.text = "\(workhrs.hours) Hr & \(workhrs.leftMinutes) mins"
            }
            else{
                self.workingHours.text = "\(workhrs.leftMinutes) mins"
            }
            
            self.elapsedTimeView.isHidden = true
            //            self.workingHours.text = bookingDetails["worked_mins"]!.stringValue
            self.bookingAddress.text = bookingDetails["address_line_1"]!.stringValue
            self.bookingTime.text = bookingDetails["timing"]!.stringValue
            self.bookingDate.text = bookingDetails["booking_date"]!.stringValue
            self.bookingId.text = bookingDetails["booking_order_id"]!.stringValue
            self.bookingName.text = bookingDetails["Name"]!.stringValue
            self.subCategoryName.text = bookingDetails["sub_category_name"]!.stringValue
            
        }
        else if(bookingDetails["status"]?.stringValue == "StarttoCustomerPlace" || bookingDetails["status"]?.stringValue == "Startedjob"){
            self.workingHours.isHidden = true
            self.taxLbl.isHidden = true
            self.tax.isHidden = true
            self.total.isHidden = true
            
            
            
            self.button.isHidden = false
            priceViewHeightConstraint.constant = 0
            priceView.layoutIfNeeded()
            scrollView.layoutIfNeeded()
            
            if(bookingDetails["status"]?.stringValue == "Startedjob")
            {
                self.updateElapsedTime()

                self.elapsedTimeView.isHidden = false
                self.button.isHidden = false
            }
            else{
                
                self.elapsedTimeView.isHidden = true
                self.button.isHidden = true
            }
            
            self.bookingAddress.text = bookingDetails["address_line_1"]!.stringValue
            self.bookingTime.text = bookingDetails["timing"]!.stringValue
            self.bookingDate.text = bookingDetails["booking_date"]!.stringValue
            self.bookingId.text = bookingDetails["booking_order_id"]!.stringValue
            self.bookingName.text = bookingDetails["Name"]!.stringValue
            self.subCategoryName.text = bookingDetails["sub_category_name"]!.stringValue
        }
        else{
            self.button.isHidden = true
            priceViewHeightConstraint.constant = 0
            priceView.layoutIfNeeded()
            scrollView.layoutIfNeeded()
            self.elapsedTimeView.isHidden = true
            
            self.bookingAddress.text = bookingDetails["address_line_1"]!.stringValue
            self.bookingTime.text = bookingDetails["timing"]!.stringValue
            self.bookingDate.text = bookingDetails["booking_date"]!.stringValue
            self.bookingId.text = bookingDetails["booking_order_id"]!.stringValue
            self.bookingName.text = bookingDetails["Name"]!.stringValue
            self.subCategoryName.text = bookingDetails["sub_category_name"]!.stringValue
        }
        
        let serviceStatusTableViewHeight = 75 * statusArray.count
        serviceStatusTableView.rowHeight = UITableViewAutomaticDimension
        serviceStatusTableView.estimatedRowHeight = 75
        
        
        serviceStatusTableView.frame = CGRect.init(x: serviceStatusTableView.frame.origin.x, y: serviceStatusTableView.frame.origin.y, width: serviceStatusTableView.frame.size.width, height: CGFloat(serviceStatusTableViewHeight))
        serviceStatusTableConstraintConstant.constant = CGFloat(serviceStatusTableViewHeight)
        serviceStatusTableView.reloadData()
        serviceStatusTableView.layoutIfNeeded()
        
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            button.backgroundColor = mycolor
            hourDigit1.backgroundColor = mycolor
            hourDigit2.backgroundColor = mycolor
            minuteDigit1.backgroundColor = mycolor
            minuteDigit2.backgroundColor = mycolor
            secondsDigit1.backgroundColor = mycolor
            secondsDigit2.backgroundColor = mycolor
        changeTintColor(imggps, arg: mycolor)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(bookingDetails["status"]?.stringValue == "Startedjob")
        {
//            self.updateElapsedTime()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(BookingDetailsViewController.updateElapsedTime), userInfo: nil, repeats: true)
        }
    }
    @objc func updateElapsedTime(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        print("Current TIME: \(formatter.string(from: Date()))")
        let currentTime = formatter.string(from: Date())
        
        let dateString = bookingDetails["job_start_time"]!.stringValue
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        
        
        let currentDateObj = dateFormatter.date(from: currentTime)!
        let dateObj = dateFormatter.date(from: dateString)!
        
        let difference = currentDateObj.offsetFrom(date: dateObj)
        if(difference.hours > 9){
            let split = splitIntoTwoDigits(input: difference.hours)
            hourDigit1.text = String(describing: split.firstDigit)
            hourDigit2.text = String(describing: split.secondDigit)
        }
        else{
            hourDigit1.text = "0"
            hourDigit2.text = String(describing: difference.hours)
        }
        
        if(difference.minutes > 9){
            let split = splitIntoTwoDigits(input: difference.minutes)
            minuteDigit1.text = String(describing: split.firstDigit)
            minuteDigit2.text = String(describing: split.secondDigit)
        }
        else{
            minuteDigit1.text = "0"
            minuteDigit2.text = String(describing: difference.minutes)
        }
        
        if(difference.seconds > 9){
            let split = splitIntoTwoDigits(input: difference.seconds)
            secondsDigit1.text = String(describing: split.firstDigit)
            secondsDigit2.text =  String(describing: split.secondDigit)
        }
        else{
            secondsDigit1.text = "0"
            secondsDigit2.text = String(describing: difference.seconds)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(timer != nil)
        {
            timer.invalidate()
        }
    }
    func splitIntoTwoDigits (input: Int) ->( firstDigit : Int, secondDigit : Int)
    {
        return (input/10,input%10)
    }
    func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ElapsedTimeViewController") as! ElapsedTimeViewController
//            vc.bookingDetails  = self.bookingDetails
//            self.present(vc, animated: true, completion: nil)
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you have finished the job?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            let bookingId = self.bookingDetails["id"]?.stringValue
            
            self.finishJob(bookingId: bookingId)
        }))
        
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
        }))
        
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func finishJob(bookingId : String!) {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let bookingId = self.bookingDetails["id"]?.stringValue
        let params: Parameters = [
            "id": bookingId!
        ]
        
        
        SwiftSpinner.show("Finishing the Job...")
        let url = "\(Constants.baseURL)/completedjob"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                self.timer.invalidate()
                if let json = response.result.value {
                    print("COMPLETE JOB JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    
                    
                    if(jsonResponse["error"].stringValue == "true")
                    {
                        let errorMessage = jsonResponse["error_message"].stringValue
                        self.showAlert(title: "Failed",msg: errorMessage)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
    func changeTintColor(_ img: UIImageView?, arg color: UIColor?) {
        if let aColor = color {
            img?.tintColor = aColor
        }
        var newImage: UIImage? = img?.image?.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions((img?.image?.size)!, false, (img?.image?.scale)!)
        color?.set()
        newImage?.draw(in: CGRect(x: 0, y: 0, width: img?.image?.size.width ?? 0.0, height: img?.image?.size.height ?? 0.0))
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        img?.image = newImage
    }
    @IBAction func cancelAction(_ sender: Any) {
        
        let bookingId = self.bookingDetails["booking_order_id"]!.stringValue
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to cancel this booking?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            print ("YES. CANCEL BOOKING ID :\(bookingId)")
            let bookingId = self.bookingDetails["id"]?.stringValue
            print(bookingId)
            self.cancelBooking(bookingId: bookingId!)
        }))
        
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            print ("NO.DONT CANCEL BOOKING ID :\(bookingId)")
        }))
        
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceStatusTableViewCell", for: indexPath) as! ServiceStatusTableViewCell
        cell.statusIdentifier.text = statusArray[indexPath.row]
        cell.statusTime.text = getReadableDate(inputDate: timeStampArray[indexPath.row])
        
        
        if(indexPath.row == 0)
        {
            cell.statusIdentifier.textColor = UIColor.lightGray
            cell.centerCircle.layer.borderWidth = 1
            cell.centerCircle.layer.borderColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0).cgColor
            cell.centerCircle.backgroundColor = UIColor.white
            cell.topLine.isHidden = true
           
            if UserDefaults.standard.object(forKey: "myColor") != nil
            {
                //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
                let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
                //            var color: UIColor? = nil
                mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
                cell.bottomLine.backgroundColor = mycolor
            }
            else{
                cell.bottomLine.isHidden = false
            }
            if(indexPath.row == statusArray.count - 1){
                
                if UserDefaults.standard.object(forKey: "myColor") != nil
                {
                    //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
                    let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
                    //            var color: UIColor? = nil
                    mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
                    cell.bottomLine.backgroundColor = mycolor
                    cell.bottomLine.isHidden = true
                }
                else{
                     cell.bottomLine.isHidden = true
                }
            }
        }
        else if(indexPath.row == statusArray.count-1)
        {
            cell.statusIdentifier.textColor = UIColor(red:0.20, green:0.21, blue:0.28, alpha:1.0)
            
            cell.centerCircle.layer.borderWidth = 1
            cell.centerCircle.layer.borderColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0).cgColor
            cell.centerCircle.backgroundColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0)
//            cell.topLine.isHidden = false
            if UserDefaults.standard.object(forKey: "myColor") != nil
            {
                //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
                let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
                //            var color: UIColor? = nil
                mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
                cell.topLine.backgroundColor = mycolor
            }
            else {
            cell.bottomLine.isHidden = true
            }
        }
        else{
            cell.statusIdentifier.textColor = UIColor.lightGray
            cell.centerCircle.layer.borderWidth = 1
            cell.centerCircle.layer.borderColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0).cgColor
            cell.centerCircle.backgroundColor = UIColor.white
            
            if UserDefaults.standard.object(forKey: "myColor") != nil
            {
                //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
                let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
                //            var color: UIColor? = nil
                mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
                cell.topLine.backgroundColor = mycolor
                cell.bottomLine.backgroundColor = mycolor
                cell.topLine.isHidden = false
                cell.bottomLine.isHidden = false
            }
            
        }
        return cell
    }
    
    func getReadableDate(inputDate : String) -> String
    {
        print(inputDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+05:30") //Current time zone
        if let date = dateFormatter.date(from: inputDate) //according to date format your date string
        {
            print(date) //Convert String to Date
            
            dateFormatter.dateFormat = "d MMM, yyyy HH:mm a" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date) //pass Date here
            
            print(newDate) //New formatted Date string
            return newDate
        }
        else{
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    @IBAction func showDirections(_ sender: Any) {
        let latitude = self.bookingDetails["user_latitude"]!.stringValue
        let longitude = self.bookingDetails["user_longitude"]!.stringValue
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
            
        } else {
            NSLog("Can't use comgooglemaps://");
            let directionsURL = "https://maps.apple.com/?saddr=&daddr=\(latitude),\(longitude)"
            guard let url = URL(string: directionsURL) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func cancelBooking(bookingId: String)
    {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let params: Parameters = [
            "id": bookingId
        ]
        print("parameters",params)
        SwiftSpinner.show("Cancelling your Booking...")
        let url = "\(Constants.baseURL)/cancelbyprovider"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("CANCEL JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                         self.showAlert(title: "Oops", msg: "The Booking has been canceled")
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
}

extension Date {

    func offsetFrom(date: Date) -> (hours: Int, minutes: Int, seconds: Int) {
        
        let hourMinuteSecond: Set<Calendar.Component> = [.hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(hourMinuteSecond, from: date, to: self);
        
        return (difference.hour!,difference.minute!,difference.second!)
    }
    
}

