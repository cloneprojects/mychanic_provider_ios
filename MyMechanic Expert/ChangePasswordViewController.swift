//
//  ChangePasswordViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            btnSubmit.backgroundColor = mycolor
        }
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submit(_ sender: Any) {
        if(oldPassword.text == "")
        {
            self.showAlert(title: "Validation Failed",msg: "Invalid Current Password")
        }
        else{
            if(password.text == "" )
            {
                self.showAlert(title: "Validation Failed",msg: "Invalid Current Password")
            }
            else if(password.text != confirmPassword.text!)
            {
                self.showAlert(title: "Validation Failed",msg: "Passwords do not match!")
            }
            else{
                
                var headers : HTTPHeaders!
                if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
                {
                    headers = [
                        "Authorization": accesstoken,
                        "Accept": "application/json"
                    ]
                }
                else
                {
                    headers = [
                        "Authorization": "",
                        "Accept": "application/json"
                    ]
                }
                
                let params: Parameters = [
                    "oldpassword": oldPassword.text!,
                    "newpassword": password.text!,
                    "cnfpassword": confirmPassword.text!
                ]
                
                
                SwiftSpinner.show("Changing Password...")
                let url = "\(Constants.baseURL)/changepassword"
                Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
                    
                    if(response.result.isSuccess)
                    {
                        SwiftSpinner.hide()
                        if let json = response.result.value {
                            print("CHANGE PASSWORD JSON: \(json)") // serialized json response
                            let jsonResponse = JSON(json)
                            
                            
                            if(jsonResponse["error"].stringValue == "true")
                            {
                                let errorMessage = jsonResponse["error_message"].stringValue
                                self.showAlert(title: "Failed",msg: errorMessage)
                            }
                            else{
                                let alert = UIAlertController(title: "Success", message: "Password Changed Successfully", preferredStyle: UIAlertControllerStyle.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                                    (alert: UIAlertAction!) in
                                    self.dismissViewController()
                                }))
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    else{
                        SwiftSpinner.hide()
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                }
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func dismissViewController()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

