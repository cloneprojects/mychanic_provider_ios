//
//  ForgotPasswordViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner


class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnnext: UIButton!
    @IBOutlet weak var btnemail: UIButton!
    @IBOutlet weak var countryCode: UIButton!
    @IBOutlet weak var phoneNumberField: UITextField!
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        
        phoneNumberField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            btnnext.backgroundColor = mycolor
            btnemail.backgroundColor = mycolor
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func goToOtpSCreen(_ sender: Any) {
        if let text = phoneNumberField.text, !text.isEmpty
        {
            let params: Parameters = [
                "email": phoneNumberField.text!
            ]
            SwiftSpinner.show("Sending OTP...")
            let url = "\(Constants.baseURL)/forgotpassword"
            Alamofire.request(url,method: .post,parameters:params).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("SEND OTP JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true")
                        {
                            let errorMessage = jsonResponse["error_message"].stringValue
                            self.showAlert(title: "Failed",msg: errorMessage)
                        }
                        else{
                            let otp = jsonResponse["otp"].stringValue
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            vc.otp = otp
                            vc.email = self.phoneNumberField.text
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
                
            }
        }
        else{
            showAlert(title: "Validation Failed",msg: "Invalid Email")
        }
        
    }
    @IBAction func showCountyCodes(_ sender: Any) {
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

