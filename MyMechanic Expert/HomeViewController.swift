//
//  HomeViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner
import Alamofire
import Nuke
import UserNotifications


class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UNUserNotificationCenterDelegate {
    var refreshControl: UIRefreshControl!
    
   

    
    
    @IBOutlet weak var tableView: UITableView!
    
    var bookings : [JSON] = []
    
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate  = self
        }
        
        // Do any additional setup after loading the view.
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:  #selector(HomeViewController.refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        getMyBookings()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.refresh(sender:)), name: NSNotification.Name(rawValue: "Refresh"), object: nil)
        

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)

    }
    override func viewWillAppear(_ animated: Bool) {
        if(MainViewController.reloadPage)
        {
            getMyBookings()
        }
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            tabBarController?.tabBar.tintColor = mycolor
        }
    }
    @objc func refresh(sender: UIRefreshControl) {
        getMyBookings()
    }
    
    @objc func changeStatus(sender:UIButton!)
    {
        let status = self.bookings[sender.tag]["status"].stringValue
        if(status == "Accepted")
        {
            print("Start to Place Clicked \(sender.tag)")
            
            var headers : HTTPHeaders!
            if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
            {
                headers = [
                    "Authorization": accesstoken,
                    "Accept": "application/json"
                ]
            }
            else
            {
                headers = [
                    "Authorization": "",
                    "Accept": "application/json"
                ]
            }
            let bookingId = self.bookings[sender.tag]["id"].stringValue
            print(bookingId)
            let params: Parameters = [
                "id": bookingId
            ]
            SwiftSpinner.show("Updating your Booking Status...")
            let url = "\(Constants.baseURL)/starttocustomerplace"
            Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    self.refreshControl.endRefreshing()
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("START TO CUSTOMER PLACE JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true" )
                        {
                            self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                        }
                        else if(jsonResponse["error"].stringValue == "Unauthenticated")
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController")     as! SigninViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            self.getMyBookings()
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                }
            }
        }
        else{
            print("Start Job \(sender.tag)")
            var headers : HTTPHeaders!
            if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
            {
                headers = [
                    "Authorization": accesstoken,
                    "Accept": "application/json"
                ]
            }
            else
            {
                headers = [
                    "Authorization": "",
                    "Accept": "application/json"
                ]
            }
            let bookingId = self.bookings[sender.tag]["id"].stringValue
            print(bookingId)
            let params: Parameters = [
                "id": bookingId
            ]
            SwiftSpinner.show("Updating your Booking Status...")
            let url = "\(Constants.baseURL)/startedjob"
            Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    self.refreshControl.endRefreshing()
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("START TO CUSTOMER PLACE JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true" )
                        {
                            self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                        }
                        else if(jsonResponse["error"].stringValue == "Unauthenticated")
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            self.getMyBookings()
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                }
            }
        }
    }
    
    
    @objc func startToPlacePressed(sender:UIButton!) {
        
        if(sender.titleLabel?.text == "SHOW DIRECTIONS"){
            let latitude = self.bookings[sender.tag]["user_latitude"].stringValue
            let longitude = self.bookings[sender.tag]["user_longitude"].stringValue
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.openURL(URL(string:
                    "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
                
            } else {
                NSLog("Can't use comgooglemaps://");
                let directionsURL = "https://maps.apple.com/?saddr=&daddr=\(latitude),\(longitude)"
                guard let url = URL(string: directionsURL) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        else{
            print("Start to Place Clicked \(sender.tag)")
            
            var headers : HTTPHeaders!
            if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
            {
                headers = [
                    "Authorization": accesstoken,
                    "Accept": "application/json"
                ]
            }
            else
            {
                headers = [
                    "Authorization": "",
                    "Accept": "application/json"
                ]
            }
            let bookingId = self.bookings[sender.tag]["id"].stringValue
            print(bookingId)
            let params: Parameters = [
                "id": bookingId
            ]
            SwiftSpinner.show("Updating your Booking Status...")
            let url = "\(Constants.baseURL)/starttocustomerplace"
            Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    self.refreshControl.endRefreshing()
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("START TO CUSTOMER PLACE JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true" )
                        {
                            self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                        }
                        else if(jsonResponse["error"].stringValue == "Unauthenticated")
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController")     as! SigninViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            self.getMyBookings()
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                }
            }
        }
    }
    func appSettings(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
            print(accesstoken)
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let url = "\(Constants.baseURL)/appsettings"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                if let json = response.result.value {
                    print("APP SETTINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        Constants.locations = jsonResponse["location"].arrayValue
                        Constants.timeSlots = jsonResponse["timeslots"].arrayValue
                        
                        let statusArray = jsonResponse["status"].arrayValue;
                        if(statusArray.count > 0){
                            print(statusArray[0])
                            if(statusArray[0]["Blocked"]["status"].stringValue == "1")
                            {
                                
                            }
                            else if(statusArray[0]["Dispute"]["status"].stringValue == "1")
                            {
                                
                            }
                            else if(statusArray[0]["Waitingforpaymentconfirmation"]["status"].stringValue == "1")
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentConfirmationViewController") as! PaymentConfirmationViewController
                               
                                vc.bookingId = statusArray[0]["Waitingforpaymentconfirmation"]["booking_id"].stringValue
                                vc.modalPresentationStyle = .overCurrentContext
                            
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(statusArray[0]["Reviewpending"]["status"].stringValue == "1")
                            {
                                if(jsonResponse["booking_details"]["isProviderReviewed"].intValue == 0)
                                {
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
                                    vc.modalPresentationStyle = .overCurrentContext
                                    vc.bookingId = statusArray[0]["Reviewpending"]["booking_id"].stringValue
                                    vc.userId = statusArray[0]["Reviewpending"]["user_id"].stringValue
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                            else if(statusArray[0]["Finished"]["status"].stringValue == "1")
                            {
                                if(jsonResponse["booking_details"]["isProviderReviewed"].intValue == 0)
                                {
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
                                    vc.modalPresentationStyle = .overCurrentContext
                                    vc.bookingId = statusArray[0]["Finished"]["booking_id"].stringValue
                                    vc.userId = statusArray[0]["Finished"]["user_id"].stringValue
                                    self.present(vc,animated:true,completion:nil)
                                }
                            }
                        }
                    }
                }
            }
            else{
                print(response.error!.localizedDescription)
                //                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func startJobPressed(sender:UIButton!) {
        print("Start Job \(sender.tag)")
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        let bookingId = self.bookings[sender.tag]["id"].stringValue
        print(bookingId)
        let params: Parameters = [
            "id": bookingId
        ]
        SwiftSpinner.show("Updating your Booking Status...")
        let url = "\(Constants.baseURL)/startedjob"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                self.refreshControl.endRefreshing()
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("START TO CUSTOMER PLACE JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.getMyBookings()
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
        
    }
    
    @objc func cancelPressed(sender:UIButton!) {
        print("Cancel Clicked \(sender.tag)")
        
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to cancel this booking?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            print ("YES. CANCEL BOOKING ID :\(self.bookings[sender.tag]["booking_order_id"].stringValue)")
            let bookingId = self.bookings[sender.tag]["id"].stringValue
            print(bookingId)
            self.cancelBooking(bookingId: bookingId)
        }))
        
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            print ("NO.DONT CANCEL BOOKING ID :\(self.bookings[sender.tag]["booking_order_id"].stringValue)")
        }))
        
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
    }
    func getMyBookings(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        SwiftSpinner.show("Fetching your Bookings...")
        let url = "\(Constants.baseURL)/homedashboard"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                self.refreshControl.endRefreshing()
                SwiftSpinner.hide()
                
                if let json = response.result.value {
                    print("BOOKINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    print(jsonResponse)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.bookings.removeAll()
                        self.bookings = jsonResponse["Accepted"].arrayValue
                        self.tableView.dataSource = self
                        self.tableView.delegate = self
                        self.tableView.reloadData()
                        self.appSettings()
                        if(jsonResponse["Pending"].arrayValue.count > 0)
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewRequestViewController") as!                            NewRequestViewController
                            
                            vc.bookingDetail = jsonResponse["Pending"][0].dictionaryValue
                             let image = jsonResponse["provider_details"]["image"].string
                            UserDefaults.standard.set(image, forKey: "image")
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            
                            self.present(vc, animated: false, completion: nil)
                        }
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelBooking(bookingId: String)
    {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let params: Parameters = [
            "id": bookingId
        ]
        SwiftSpinner.show("Cancelling your Booking...")
        let url = "\(Constants.baseURL)/cancelbyprovider"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                self.refreshControl.endRefreshing()
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("CANCEL JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.getMyBookings()
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTableViewCell", for: indexPath) as! BookingTableViewCell
        cell.providerName.text = self.bookings[indexPath.row]["Name"].stringValue
        cell.subCategoryName.text = self.bookings[indexPath.row]["sub_category_name"].stringValue
        cell.serviceDate.text = self.bookings[indexPath.row]["booking_date"].stringValue
         cell.serviceTime.text = self.bookings[indexPath.row]["timing"].stringValue
        if let imageURL = URL.init(string:self.bookings[indexPath.row]["userimage"].stringValue)
        {
            Nuke.loadImage(with: imageURL, into: cell.providerImage)
        }
        let status = self.bookings[indexPath.row]["status"].stringValue
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.changeStatusButton.tag = indexPath.row
        
        if #available(iOS 11.0, *) {
            cell.cornerView.clipsToBounds = true
            cell.cornerView.layer.cornerRadius = 10
            
            cell.cornerView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
            cell.cornerView.clipsToBounds = true
            let maskPath = UIBezierPath(roundedRect: cell.cornerView.bounds,
                                        byRoundingCorners: [.bottomRight],
                                        cornerRadii: CGSize(width: 10.0, height: 10.0))
            let shape = CAShapeLayer()
            shape.path = maskPath.cgPath
            cell.cornerView.layer.mask = shape
            
        }
        
        if(status == "Pending")
        {
            //--
            cell.bookingStatusImageView.image = UIImage(named: "pending_red")
            cell.bookingStatusLbl.text = "Pending"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            
            cell.cornerView.isHidden = true
            cell.changeStatusButton.isHidden = true
            cell.changeStatusButton.isUserInteractionEnabled = false
            
            //--
            
        }
        else if(status == "Rejected")
        {
            //--
            cell.bookingStatusImageView.image = UIImage(named: "new_cancelled")
            cell.bookingStatusLbl.text = "Rejected"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            
            
            
            cell.cornerView.isHidden = true
            
            
        }
        else if(status == "Accepted")
        {
            
            //--
            cell.bookingStatusImageView.image = UIImage(named: "finished_green")
            cell.bookingStatusLbl.text = "Accepted"
            cell.bookingStatusLbl.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            
            cell.cornerView.isHidden = false
            cell.cornerView.backgroundColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
//            cell.changeStatusButton.tag = 1
            cell.changeStatusLbl.text = "START TO PLACE"
            cell.changeStatusButton.addTarget(self, action: #selector(self.changeStatus(sender:)), for: .touchUpInside)
            
            //--
            
        }
        else if(status == "CancelledbyUser")
        {
            
            //--
            cell.bookingStatusImageView.image = UIImage(named: "new_cancelled")
            cell.bookingStatusLbl.text = "Cancelled by User"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            
            cell.cornerView.isHidden = true
            //--
            

            
        }
        else if(status == "CancelledbyProvider")
        {
            //--
            cell.bookingStatusImageView.image = UIImage(named: "new_cancelled")
            cell.bookingStatusLbl.text = "Cancelled by Provider"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            
            cell.cornerView.isHidden = true
            
            //==
           
        }
        else if(status == "StarttoCustomerPlace")
        {
            //--
            cell.bookingStatusImageView.image = UIImage(named: "start_to_place")
            cell.bookingStatusLbl.text = "On the way"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            
            cell.cornerView.isHidden = false
            cell.changeStatusLbl.text = "START JOB"
            
            cell.cornerView.backgroundColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            
            cell.changeStatusButton.addTarget(self, action: #selector(self.changeStatus(sender:)), for: .touchUpInside)

//            cell.changeStatusButton.tag = 2
            //--
           
            
        }
        else if(status == "Startedjob")
        {
            
            //--
            cell.bookingStatusImageView.image = UIImage(named: "start_job")
            cell.bookingStatusLbl.text = "Work in progress"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            
            
            
            cell.cornerView.isHidden = true
            //--
            

            
            
        }
        else if(status == "Completedjob")
        {
            
            //--
            cell.bookingStatusImageView.image = UIImage(named: "pay")
            cell.bookingStatusLbl.text = "Payment pending"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            
            cell.cornerView.isHidden = true
            //--
            
          
            
        }
        else if(status == "Waitingforpaymentconfirmation")
        {
            //--
            cell.bookingStatusImageView.image = UIImage(named: "pay")
            cell.bookingStatusLbl.text = "Payment pending"
            cell.bookingStatusLbl.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            
            cell.cornerView.isHidden = true
            //--

        }
        else if(status == "Reviewpending")
        {
            cell.bookingStatusImageView.image = UIImage(named: "new_finished")
            cell.bookingStatusLbl.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            cell.cornerView.isHidden = true

            if(self.bookings[indexPath.row]["isProviderReviewed"].intValue == 0)
            {
                cell.bookingStatusLbl.text = "Review pending"
            }
            else{
                cell.bookingStatusLbl.text = "Job Completed"
            }
            //==-

        }
        else if(status == "Finished")
        {
            //--
            cell.bookingStatusImageView.image = UIImage(named: "new_finished")
            cell.bookingStatusLbl.text = "Job Completed"
            cell.bookingStatusLbl.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            
            cell.cornerView.isHidden = true
            //--
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsViewController") as! BookingDetailsViewController
        MainViewController.changePage = false
        vc.bookingDetails = self.bookings[indexPath.row].dictionary
        self.present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookings.count
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
    }    
    
}

