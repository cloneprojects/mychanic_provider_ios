//
//  MainViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import SwiftyJSON
import UserNotifications
import GoogleMaps




class MainViewController: UITabBarController, UITabBarControllerDelegate,UNUserNotificationCenterDelegate {
    static var changePage = true
    static var reloadPage = false
    static var status : [JSON]! = []
    var mycolor = UIColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
    
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate  = self
        }
        

        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(MainViewController.changePage)
        {
            self.selectedIndex = 0
        }
        
        if(MainViewController.reloadPage)
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refresh"), object: self)
            MainViewController.reloadPage = false
        }
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            tabBarController?.tabBar.tintColor = mycolor
        }
        
        
        
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
    }
    
}
