//
//  ResetPasswordViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON


class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var confrimPasswordFld: UITextField!
    var email : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func goToBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func resetPassword(_ sender: Any) {
        let pass = passwordFld.text!
        let confirmpass = confrimPasswordFld.text!
        
        if(pass.count == 0)
        {
            showAlert(title: "Validation Failed", msg: "Please enter a vaild Password")
        }
        else{
            if(pass == confirmpass)
            {
                let params: Parameters = [
                    "email": self.email,
                    "confirmpassword": confirmpass,
                    "password": pass
                ]
                SwiftSpinner.show("Changing your password...")
                let url = "\(Constants.baseURL)/resetpassword"
                Alamofire.request(url,method: .post,parameters:params).responseJSON { response in
                    
                    if(response.result.isSuccess)
                    {
                        SwiftSpinner.hide()
                        if let json = response.result.value {
                            print("RESET PASSWORD JSON: \(json)") // serialized json response
                            let jsonResponse = JSON(json)
                            if(jsonResponse["error"].stringValue == "true")
                            {
                                let errorMessage = jsonResponse["error_message"].stringValue
                                self.showAlert(title: "Login Failed",msg: errorMessage)
                            }
                            else{
                                let alert = UIAlertController(title: "Password Changed Successfully", message: "Please login", preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                                    (alert: UIAlertAction!) in
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                                    self.present(vc, animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    else{
                        SwiftSpinner.hide()
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                }
            }
            else{
                showAlert(title: "Validation Failed", msg: "Passwords do not match!")
            }
        }
    }
    
}

