//
//  SchedulesViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 15/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SwiftSpinner

class SchedulesViewController: UIViewController,TimeSelectionDelegate {

    @IBOutlet weak var btnUpate: UIButton!
    var monday : [JSON] = []
    var tuesday : [JSON] = []
    var wednesday : [JSON] = []
    var thursday : [JSON] = []
    var friday : [JSON] = []
    var saturday : [JSON] = []
    var sunday : [JSON] = []
    var timeSlots : [JSON] = []

    @IBOutlet weak var mondayAddButton: UIButton!
    @IBOutlet weak var mondayLbl: UILabel!
    
    @IBOutlet weak var tuesdayAddButton: UIButton!
    @IBOutlet weak var tuesdayLbl: UILabel!
    
    @IBOutlet weak var wednesdayLbl: UILabel!
    @IBOutlet weak var wednesdayAddButton: UIButton!
    
    @IBOutlet weak var thursdayAddButton: UIButton!
    @IBOutlet weak var thursdayLbl: UILabel!
    
    @IBOutlet weak var fridayAddButton: UIButton!
    @IBOutlet weak var fridayLbl: UILabel!
    
    @IBOutlet weak var saturdayAddButton: UIButton!
    @IBOutlet weak var saturdayLbl: UILabel!
    
    @IBOutlet weak var sundayAddButton: UIButton!
    @IBOutlet weak var sundayLbl: UILabel!
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            
            mondayAddButton.titleLabel?.textColor = mycolor
            tuesdayAddButton.titleLabel?.textColor = mycolor
            wednesdayAddButton.titleLabel?.textColor = mycolor
            thursdayAddButton.titleLabel?.textColor = mycolor
            fridayAddButton.titleLabel?.textColor = mycolor
            saturdayAddButton.titleLabel?.textColor = mycolor
            sundayAddButton.titleLabel?.textColor = mycolor
            btnUpate.backgroundColor = mycolor
        }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.getMySchedules()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func getMySchedules(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        SwiftSpinner.show("Fetching your Schedule...")
        let url = "\(Constants.baseURL)/view_schedules"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("BOOKINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.timeSlots = jsonResponse["schedules"].arrayValue
                        if(self.timeSlots.count == 0)
                        {
                            for i in 0 ... Constants.timeSlots.count-1{
                                for dayOfTheWeek in 0 ... 6{
                                    var day:JSON = JSON.init()
                                    switch(dayOfTheWeek){
                                    case 0:
                                        day["days"].stringValue = "Mon"
                                        break
                                    case 1:
                                        day["days"].stringValue = "Tue"
                                        break
                                    case 2:
                                        day["days"].stringValue = "Wed"
                                        break
                                    case 3:
                                        day["days"].stringValue = "Thu"
                                        break
                                    case 4:
                                        day["days"].stringValue = "Fri"
                                        break
                                    case 5:
                                        day["days"].stringValue = "Sat"
                                        break
                                    case 6:
                                        day["days"].stringValue = "Sun"
                                        break
                                    default:
                                        day["days"].stringValue = "Sun"
                                    }
                                    
                                    day["status"].stringValue = "0"
                                    day["time_Slots_id"].stringValue = Constants.timeSlots[i]["id"].stringValue
                                    self.timeSlots.append(day)
                                }
                            }
                        }
                        self.displaySlotsInLabel()
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }

    }

    @IBAction func updateSchedule(_ sender: Any) {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let schedule = String(describing: self.timeSlots)
        let params: Parameters = [
            "schedules":schedule
        ]
        SwiftSpinner.show("Updating Schedules...")
        let url = "\(Constants.baseURL)/updateschedules"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("UPDATE SCHEDULE JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        
                        self.dismiss(animated: true, completion: nil);
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    @IBAction func addTime(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimeSlotViewController") as! SelectTimeSlotViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.forDay = sender.tag
        switch sender.tag{
        case 0:
            vc.daysData = monday
            break;
        case 1:
            vc.daysData = tuesday
            break;
        case 2:
            vc.daysData = wednesday
            break;
        case 3:
            vc.daysData = thursday
            break;
        case 4:
            vc.daysData = friday
            break;
        case 5:
            vc.daysData = saturday
            break;
        case 6:
            vc.daysData = sunday
            break;
        default:
            vc.daysData = monday
            break;
        }
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func displaySlotsInLabel(){
        
        monday.removeAll()
        tuesday.removeAll()
        wednesday.removeAll()
        thursday.removeAll()
        friday.removeAll()
        saturday.removeAll()
        
      for i in 0 ... self.timeSlots.count - 1
      {
        let slot = self.timeSlots[i]
        let slotsData = self.timeSlots[i].dictionaryValue
        print(slotsData)
        let dayOfTheWeek = slotsData["days"]!.stringValue
        print(dayOfTheWeek)
        
            switch dayOfTheWeek {
            case "Mon":
                monday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: mondayLbl.text!)
                    if(mondayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    mondayLbl.text = timingString
                    if(mondayLbl.text!.count > 0)
                    {
                        self.mondayAddButton.titleLabel?.text = ""
                    }
                }
                break
            case "Tue":
                tuesday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: tuesdayLbl.text!)
                    if(tuesdayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }

                    tuesdayLbl.text = timingString
                    if(tuesdayLbl.text!.count > 0)
                    {
                        self.tuesdayAddButton.titleLabel?.text = ""
                    }
                }
                break
            case "Wed":
                wednesday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: wednesdayLbl.text!)
                
                    if(wednesdayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                
                    wednesdayLbl.text = timingString
                    if(wednesdayLbl.text!.count > 0)
                    {
                        self.wednesdayAddButton.titleLabel?.text = ""
                    }
                }
                break
            case "Thu":
                thursday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: thursdayLbl.text!)

                    if(thursdayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    
                    thursdayLbl.text = timingString
                    if(thursdayLbl.text!.count > 0)
                    {
                        self.thursdayAddButton.titleLabel?.text = ""
                    }
                }
                    break
            case "Fri":
                friday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: fridayLbl.text!)
                    if(fridayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                
                    fridayLbl.text = timingString
                    if(fridayLbl.text!.count > 0)
                    {
                        self.fridayAddButton.titleLabel?.text = ""
                    }
                }
                break
            case "Sat":
                saturday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: saturdayLbl.text!)
                    if(saturdayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                
                    saturdayLbl.text = timingString
                    if(saturdayLbl.text!.count > 0)
                    {
                        self.saturdayAddButton.titleLabel?.text = ""
                    }
                }
                break
            case "Sun":
                sunday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: sundayLbl.text!)
                    if(sundayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                
                    sundayLbl.text = timingString
                    if(sundayLbl.text!.count > 0)
                    {
                        self.sundayAddButton.titleLabel?.text = ""
                    }
                }
                break
            default:
                monday.append(slot)
                if(slotsData["status"]!.intValue == 1){
                    var timingString = String(describing: mondayLbl.text!)
                
                    if(mondayLbl.text == "")
                    {
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                    else{
                        timingString.append(", ")
                        let currentTimeSlot = slotsData["timing"]!.stringValue
                        timingString.append(currentTimeSlot)
                    }
                
                    mondayLbl.text = timingString
                    if(mondayLbl.text!.count > 0)
                    {
                        self.mondayAddButton.titleLabel?.text = ""
                    }
                }
            }
        }
    }
    
    func changeStatus(timeSlotId: String, dayOfTheWeek: Int, status: String){
        var count = 0;
        for i in 0 ... Constants.timeSlots.count-1{
            for dotw in 0 ... 6{
                if(Constants.timeSlots[i]["id"].stringValue == timeSlotId && dotw == dayOfTheWeek){
                    self.timeSlots[count]["status"].stringValue = status
                }
                count = count + 1
            }
        }
        print(self.timeSlots)
    }
    
    func didFinishSelectingTime(slotsData: [JSON], dayOfTheWeek : Int) {
        switch dayOfTheWeek {
        case 0:
            monday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(monday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(monday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            mondayLbl.text = displayString
            if(mondayLbl.text!.count > 0)
            {
                self.mondayAddButton.titleLabel?.text = ""
            }
            
        case 1:
            tuesday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(tuesday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(tuesday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            tuesdayLbl.text = displayString
            if(tuesdayLbl.text!.count > 0)
            {
                self.tuesdayAddButton.titleLabel?.text = ""
            }
        case 2:
            wednesday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(wednesday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(wednesday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            wednesdayLbl.text = displayString
            if(wednesdayLbl.text!.count > 0)
            {
                self.wednesdayAddButton.titleLabel?.text = ""
            }
            
        case 3:
            thursday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(thursday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(thursday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            thursdayLbl.text = displayString
            if(thursdayLbl.text!.count > 0)
            {
                self.thursdayAddButton.titleLabel?.text = ""
            }
        case 4:
            friday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(friday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(friday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            fridayLbl.text = displayString
            if(fridayLbl.text!.count > 0)
            {
                self.fridayAddButton.titleLabel?.text = ""
            }
        case 5:
            saturday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(saturday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(saturday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            saturdayLbl.text = displayString
            if(saturdayLbl.text!.count > 0)
            {
                self.saturdayAddButton.titleLabel?.text = ""
            }
            
        case 6:
            sunday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(sunday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(sunday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            sundayLbl.text = displayString
            if(sundayLbl.text!.count > 0)
            {
                self.sundayAddButton.titleLabel?.text = ""
            }
            
        default:
            monday = slotsData
            
            var displayString = ""
            for i in 0 ... Constants.timeSlots.count-1
            {
                if(displayString == "")
                {
                    if(monday[i]["status"] == "1"){
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                else{
                    
                    if(monday[i]["status"] == "1"){
                        displayString.append(", ")
                        let currentTimeSlot = Constants.timeSlots[i]["timing"].stringValue
                        displayString.append(currentTimeSlot)
                    }
                }
                let slotId = slotsData[i]["time_Slots_id"].stringValue
                let status = slotsData[i]["status"].stringValue
                self.changeStatus(timeSlotId: slotId, dayOfTheWeek: dayOfTheWeek, status: status)
            }
            
            mondayLbl.text = displayString
            if(mondayLbl.text!.count > 0)
            {
                self.mondayAddButton.titleLabel?.text = ""
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
