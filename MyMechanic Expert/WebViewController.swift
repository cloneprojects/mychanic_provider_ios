//
//  WebViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 07/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import SwiftyJSON
import SwiftSpinner

class WebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var webView: UIWebView!
    var titleString: String!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = titleString
        
        webView.delegate = self
        
        if(titleString == "About Us")
        {
            getWebContent(key: "aboutus")
        }
        else if(titleString == "Help and FAQ")
        {
            getWebContent(key: "faq")
        }
        else if(titleString == "Terms & Conditions")
        {
            getWebContent(key: "terms")
        }
        // Do any additional setup after loading the view.
    }

    func getWebContent(key:String){
        
        SwiftSpinner.show("Loading...")
        let url = "\(Constants.adminBaseURL)/\(key)"
        Alamofire.request(url,method: .get).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("WEB JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "Unauthenticated" || jsonResponse["error"].stringValue == "true")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let pageUrlString = jsonResponse["static_pages"][0]["page_url"].stringValue
                        let pageUrl = URL.init(string: pageUrlString)
                        let urlRequest = URLRequest.init(url: pageUrl!)
                        self.webView.loadRequest(urlRequest)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
            }
        }
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SwiftSpinner.show("Loading")
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        SwiftSpinner.hide()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SwiftSpinner.hide()
        self.showAlert(title: "Oops", msg: error.localizedDescription)
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftSpinner.hide()
    }
}
